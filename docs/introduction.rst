.. _introduction:

Introduction to Django-Plotly-Wagtail
=====================================

This package provides a link between Plotly and Wagtail, by leveraging
the django-plotly-dash package.

