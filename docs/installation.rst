.. _installation:


Installation
============

To install Django-Plotly-Wagtail use pip:

.. code-block:: console

   pip install django-plotly-wagtail

   
This will install the package along with its dependents.


Prerequisites
-------------

Django-Plotly-Wagtail works in conjunction with wagtail itself and on Django
versions greater than 3.2. It is not supported on versions of python earlier
than 3.9
