.. _demo:

Demonstration Server
====================

The repository contains a complete demonstration server in the demo subdirectory.


Settting up the demo server
---------------------------

From the root directory of a clone of the git repository, the
script ``./scripts/setup_demo_server.bash`` will set up the environment and
then ``./scripts/run_demo_server.bash`` can be used to launch the
demo server on http://localhost:8000/

Initially, this will show a blank page, as the CMS has no content.

Before adding content, at least one Dash app needs to be added. First navigate
to the admin section of the page at http://localhost:8000/admin/ which is one
of the helpful links at the top of the page. Under 'DJANGO PLOTLY DASH' on the
LHS, select `Stateless apps` and then use
the `Find Apps` button on the RHS to add any apps visible to the server. This should
include at least the `SimpleExample` app.

Now, under 'DPWAGTAIL' on the LHS, select `Dash Instances` and then use
the `Add Dash Instance` button on the RHS to create a new instance. Any name can be
used, and their should be at least one entry in each of
the `Instance builder` and `Stateless app` dropdowns.

The next step is to manually add content. Navigate to http://localhost:8000/cms/ (you should need to login
with superuser credentials, such as those in the ``scripts/config.py`` file). This is
also one of the helpful links at the top of the page.

Under the pages element on the left hand side is listed the possible root elements, one
of which is the DPD Index page. Click on this link and then add child
page. Select `Django plotly dash page` and then create a new page. The instance created above
should be present in the dropdown. Once created, the 'Save Draft' dropdown at the bottom can be
used to publish the new page.

Return to the front page and the note the presence of a link to the example with
the short description below it. Clicking on the link should take you to the actual
application accompanied by the long description.
