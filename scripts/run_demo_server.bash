#!/usr/bin/env bash
#
# Run the demo server for interactive testing
#
source scripts/start.bash
#
cd demo
#
python manage.py migrate
python manage.py "$@"
