#!/usr/bin/env bash
#
# Run the tests
#
source scripts/start.bash
#
cd demo
pytest demo --cov=demo --cov=dpwagtail --cov-report term-missing
