#!/usr/bin/env bash
#
# Build and launch the demo server
#
source scripts/env_vars.bash
./scripts/make_env.bash
# In case anything is added to the startup for the environment
./scripts/config_demo_server.bash
