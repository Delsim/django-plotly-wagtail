"""
URL configuration for demo project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from django.views.generic import TemplateView


from .views import (instance_template_tag_render,
                    predefined_render, predefined_render_2,
                    predefined_render_3,
                    )


urlpatterns = [
    path('admin/', admin.site.urls),
    path('dpwagtail/', include('dpwagtail.urls')),
    path('tv/1', instance_template_tag_render, {'template_name': "tv_1.html"}),   
    path('tv/2', TemplateView.as_view(template_name="tv_2.html")),
    path('tv/3', predefined_render, {'template_name': "tv_1.html"}),
    path('tv/4', predefined_render_2, {'template_name': "tv_1.html"}),
    path('tv/5', predefined_render_3, {'template_name': "tv_1.html"}),
    path('dpdash/', include('django_plotly_dash.urls')), 
]

# Include wagtail urls
urlpatterns += [
    path('cms/', include('wagtail.admin.urls')),
    path('docs/', include('wagtail.documents.urls')),
    path('pages', include('wagtail.urls')),
    path('', include('wagtail.urls')),
]
